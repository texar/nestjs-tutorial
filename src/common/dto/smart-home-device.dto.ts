import { IsNumber, IsPositive } from 'class-validator';

export class SmartHomeDeviceDto {
  @IsPositive()
  relayNo: number;

  @IsNumber()
  state: number;
}
