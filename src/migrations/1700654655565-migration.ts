import { MigrationInterface, QueryRunner } from 'typeorm';

export class Migration1700654655565 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
          `ALTER TABLE "coffee" RENAME COLUMN "name" TO "title"`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
          `ALTER TABLE "coffee" RENAME COLUMN "title" TO "name"`,
        );
    }

}
