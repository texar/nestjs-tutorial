import { Module } from '@nestjs/common';
import { SmartHomeController } from './smart-home.controller';
import { SmartHomeService } from './smart-home.service';

@Module({
  controllers: [SmartHomeController],
  providers: [SmartHomeService],
})
export class SmartHomeModule {}
