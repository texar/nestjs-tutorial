import { HttpException, Injectable } from '@nestjs/common';
import { SmartHomeDeviceDto } from '../common/dto/smart-home-device.dto';
import * as five from 'johnny-five';

@Injectable()
export class SmartHomeService {
  private board = this.initBoard();
  private isReady = false;
  private relays: five.Relays;

  private initBoard(): five.Board {
    this.board = new five.Board();

    this.board.on('ready', () => {
      this.relays = new five.Relays([
        {
          pin: 7,
          type: 'NC',
        },
        {
          pin: 8,
          type: 'NC',
        },
      ]);

      this.board.repl.inject({
        relays: this.relays,
      });
      this.isReady = true;
    });

    this.board.on('fail', (event) => {
      /*
        Event {
          type: "info"|"warn"|"fail",
          timestamp: Time of event in milliseconds,
          class: name of relevant component class,
          message: message [+ ...detail]
        }
      */
      console.log("%s sent a 'fail' message: %s", event.class, event.message);
      this.isReady = false;
    });

    // This event is emitted synchronously on SIGINT. Use this handler to do any necessary cleanup before your program is "disconnected" from the board.
    this.board.on('exit', () => {
      console.log('exit');
      this.isReady = false;
    });

    return this.board;
  }

  setRelayState(query: SmartHomeDeviceDto) {
    if (!this.isReady) {
      throw new HttpException('Board is not ready', 500);
    }

    if (!this.relays) {
      throw new HttpException('Relays are not initialized', 500);
    }

    if (query.state === 1) {
      this.relays[query.relayNo - 1].close();
    } else {
      this.relays[query.relayNo - 1].open();
    }

    return query;
  }

  pushTheButton(query: SmartHomeDeviceDto): string {
    // TODO: dont push the button if its already pushed
    this.relays[query.relayNo - 1].close();
    setTimeout(() => {
      this.relays[query.relayNo - 1].open();
    }, 1000);

    return 'OK!';
  }
}
