import { Controller, Get, Patch, Query } from '@nestjs/common';
import { SmartHomeDeviceDto } from '../common/dto/smart-home-device.dto';
import { SmartHomeService } from './smart-home.service';

@Controller('smart-home')
export class SmartHomeController {
  constructor(private readonly smartHomeService: SmartHomeService) {}
  @Get('status')
  getState() {
    return 'OK!';
  }

  @Get('push-the-button')
  pushTheButton(@Query() query: SmartHomeDeviceDto) {
    return this.smartHomeService.pushTheButton(query);
  }

  @Patch('set-relay-state')
  setRelayState(@Query() query: SmartHomeDeviceDto) {
    return this.smartHomeService.setRelayState(query);
  }
}
