import { DataSource } from 'typeorm';

import { Flavor } from './src/coffees/entities/flavor.entity';
import { Coffee } from './src/coffees/entities/coffee.entity';
import { SchemaSync1700655865063 } from './src/migrations/1700655865063-SchemaSync';

export default new DataSource({
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'postgres',
  password: 'pass123',
  database: 'postgres',
  entities: [Coffee, Flavor],
  migrations: [SchemaSync1700655865063],
});
